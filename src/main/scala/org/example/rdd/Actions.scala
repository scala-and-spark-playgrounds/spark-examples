package org.example.rdd

import org.apache.spark.sql.SparkSession

object Actions extends App {

  val spark = SparkSession.builder()
    .appName("SparkByExample")
    .master("local")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  val inputRDD = spark.sparkContext.parallelize(List(("Z", 1),("A", 20),("B", 30),("C", 40),("B", 30),("B", 60)))

  val listRdd = spark.sparkContext.parallelize(List(1,2,3,4,5,3,2))

  def param1 = (acc: Int, v: Int) => acc + v
  def param2 = (acc1: Int, acc2: Int) => acc1 + acc2

  // println(listRdd.aggregate(0)(param1, param2))

  def param3 = (acc: Int, v: (String, Int)) => acc + v._2
  def param4 = (acc1: Int, acc2: Int) => acc1 + acc2

  // println(inputRDD.aggregate(0)(param3, param4))


  /*println(listRdd.fold(0)((acc, v) => {
    acc + v
  }))

  println(inputRDD.fold(("Total", 0))((acc, v) => {
    ("Total", v._2 + acc._2)
  }))*/

  println(listRdd.reduce(_ + _))

  println(inputRDD.reduce((a, b) => ("Total", a._2 + b._2)))




}
