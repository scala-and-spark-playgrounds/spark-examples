package org.example.dataframes

import org.apache.spark.sql.SparkSession

object JosnTest extends App {



  val spark = SparkSession.builder()
    .appName("SparkByExample")
    .master("local")
    .getOrCreate()

  val jsonString="""{"Zipcode":704,"ZipCodeType":"STANDARD","City":"PARC PARQUE","State":"PR"}"""
  val data = Seq((1, jsonString))

  import spark.implicits._

  val df = data.toDF("id", "value")


  import org.apache.spark.sql.functions.{from_json,col, json_tuple}
  import org.apache.spark.sql.types.{MapType, StringType}

  val df2 = df.withColumn("value", from_json(col("value"), MapType(StringType, StringType)))

  //df2.show(false)

  df.select(col("id"),json_tuple(col("value"),"Zipcode","ZipCodeType","City"))
    .toDF("id", "Zipcode", "ZipCodeType", "City")
    .show(false)

}
