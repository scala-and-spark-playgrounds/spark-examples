package org.example.dataframes

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.col

object UDF extends App {

  val spark: SparkSession = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExamples.com")
    .getOrCreate()

  import spark.implicits._
  val columns = Seq("Seqno","Quote")
  val data = Seq(
    ("1", "Be the change that you wish to see in the world"),
    ("2", "Everyone thinks of changing the world, but no one thinks of changing himself."),
    ("3", "The purpose of our lives is to be happy.")
  )
  val df = data.toDF(columns:_*)

  var fun = (str: String) => {
    str.split(" ").map(s => s.capitalize).mkString(" ")
  }

  val myudf = udf(fun)

  df
    .select(
      col("Seqno"),
      myudf(col("Quote")).as("Quote")
    )
    .show(false)
}
