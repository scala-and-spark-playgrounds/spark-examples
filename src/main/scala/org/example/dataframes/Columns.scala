package org.example.dataframes

import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructType}

object Columns extends App {

  val spark = SparkSession.builder()
    .appName("SparkByExample")
    .master("local")
    .getOrCreate()

  val data = Seq(Row(Row("James;","","Smith"),"36636","M","3000"),
    Row(Row("Michael","Rose",""),"40288","M","4000"),
    Row(Row("Robert","","Williams"),"42114","M","4000"),
    Row(Row("Maria","Anne","Jones"),"39192","F","4000"),
    Row(Row("Jen","Mary","Brown"),"","F","-1")
  )

  val schema = new StructType()
    .add("name",new StructType()
      .add("firstname",StringType)
      .add("middlename",StringType)
      .add("lastname",StringType))
    .add("dob",StringType)
    .add("gender",StringType)
    .add("salary",StringType)

  val df = spark.createDataFrame(spark.sparkContext.parallelize(data), schema)

  val df2 =
    df.withColumn("Country", lit("USA"))
    .withColumn("Salary", col("Salary") * 100)

  df2.printSchema()
  df2.show(false)

  df.createOrReplaceTempView("PERSON")

  spark.sql("SELECT Salary * 100 as Salary, 'USA' as Country FROM PERSON")
    .show(false)

}
