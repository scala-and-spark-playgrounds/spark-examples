package org.example.dataframes

import org.apache.spark.sql.SparkSession

object WindowTest extends App {

  val spark = SparkSession.builder()
    .appName("SparkByExample")
    .master("local")
    .getOrCreate()



  import spark.implicits._

  val simpleData = Seq(("James", "Sales", 3000),
    ("Michael", "Sales", 4600),
    ("Robert", "Sales", 4100),
    ("Maria", "Finance", 3000),
    ("James", "Sales", 3000),
    ("Scott", "Finance", 3300),
    ("Jen", "Finance", 3900),
    ("Jeff", "Marketing", 3000),
    ("Kumar", "Marketing", 2000),
    ("Saif", "Sales", 4100)
  )
  val df = simpleData.toDF("employee_name", "department", "salary")

  import org.apache.spark.sql.functions._

  import org.apache.spark.sql.expressions.Window


  val winSpec = Window.partitionBy("department").orderBy("Salary");

  df.withColumn("row_number", dense_rank().over(winSpec))
    .show()


}
