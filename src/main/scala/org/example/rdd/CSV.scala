package org.example.rdd

import org.apache.spark.sql.SparkSession

object CSV extends App {
  val spark = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExample")
    .getOrCreate();

  val rdd = spark.sparkContext.textFile("C:/data/kv.csv")

  val rdd2 = rdd.map(_.split(",")).mapPartitionsWithIndex((i, itr) => {
    if (i == 0) itr.drop(1)
    else itr;
  })

  rdd2.foreach(f => println(s"${f(0)} - ${f(1)}"))

}
