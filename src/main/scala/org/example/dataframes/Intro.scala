package org.example.dataframes

import org.apache.spark.sql.SparkSession

object Intro extends App {

  val spark = SparkSession.builder()
    .appName("SparkByExample")
    .master("local")
    .getOrCreate()

  import spark.implicits._
  val columns = Seq("language","users_count")
  val data = Seq(("Java", "20000"), ("Python", "100000"), ("Scala", "3000"))

  val rdd = spark.sparkContext.parallelize(data);

  val df1 = rdd.toDF(columns:_*);

  df1.printSchema()

  val df2 = spark.read.csv("C:/data/zipcodes.json")

  df2.printSchema()

}
