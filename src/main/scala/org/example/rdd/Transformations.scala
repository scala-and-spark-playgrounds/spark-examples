package org.example.rdd

import org.apache.spark.sql.SparkSession

object Transformations extends App {

  val spark = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExample")
    .getOrCreate();

  val rdd = spark.sparkContext.textFile("C:/data/test.txt");

  rdd.flatMap(_.split(" "))
    .map((_, 1))
    .filter(_._1.startsWith("a"))
    .reduceByKey(_ + _)
    .map(a => (a._2, a._1))
    .sortByKey()
    .foreach(println)

}
