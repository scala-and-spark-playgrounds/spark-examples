package org.example.rdd

import org.apache.spark.sql.SparkSession

object Text extends App {
  val spark = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExample")
    .getOrCreate();

  val rdd = spark.sparkContext.textFile("C:/data/csv/text*.txt")

  rdd.map(_.split(",")).foreach(f => println(s"${f(0)} - ${f(1)}"))
}
