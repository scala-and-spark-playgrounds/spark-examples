package org.example.rdd

import org.apache.spark.sql.SparkSession

object PairRDD extends App {

  val spark = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExample")
    .getOrCreate();

  val rdd = spark.sparkContext.parallelize(
    List("Germany India USA","USA India Russia","India Brazil Canada China")
  )

  rdd
    .flatMap(_.split(" "))
    .map((_, 1))
    .sortByKey()
    .reduceByKey(_ + _)
    .map(a => (a._2, a._1))
    .sortByKey(true)
    .foreach(println)

  rdd
    .flatMap(_.split(" "))
    .map((_, 1))
    .groupByKey()
    .foreach(println)

}
