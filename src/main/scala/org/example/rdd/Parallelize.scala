package org.example.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object Parallelize extends App {
  val spark = SparkSession.builder()
    .master("local[2]")
    .appName("SparkByExample")
    .getOrCreate();

  val sc = spark.sparkContext

  val par: RDD[Int] = sc.parallelize(List(1,2,3,4,5,6,7))

  println(par.getNumPartitions)
  println(par.first())

  par.collect().foreach(println)
}
